# order

Running:

1. `gradlew bootRun`
2. runinng test: `gradlew test`

Storage:
Memory based H2 databasse

API


```yaml
Endpoint:
    '/products':
        GET: 
          description: gets all Products
        POST:
          description: create new Product
        PUT:
          description: updates product
          pathParameter:
            {id}: id of product
          
    '/orders':
        GET: 
          description: gets all placed orders in time
          requestParameters:
            start: 
              type: String
              Format: YYYY-MM-dd
            end: 
              type: String
              Format: YYYY-MM-dd
            
        POST:
          description: create new order
          body:
            accepted: appplication/json
            schema: '{email: String}'
     '/orders/{orderid}/total':
        GET: 
          description: calculates total order value
          pathParameter:
            {orderId}: id of product
     '/orders/{orderid}/place':
        PUT: 
          description: places an order so it cannot be changed in future
          pathParameter:
            {orderId}: id of product    
     '/orders/{orderid}/{productId}':
        POST: 
          description: Adds product to order
          pathParameter:
            {orderId}: id of product
```