package com.product.order.orders;

import com.product.order.product.Product;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "orders")
class Order {

    @Id
    @GeneratedValue
    private long id;
    private LocalDateTime placementDate;
    @NotEmpty
    private String email;
    @ManyToMany
    private List<Product> productsInOrder= new ArrayList<>();
    private BigDecimal orderAmount;


    BigDecimal calculateOrderTotal() {
        if (placementDate != null) {
            return orderAmount;
        }
        return productsInOrder.stream()
                .map(Product::getPrice)
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }

    public Order place() {
        if (placementDate == null) {
            orderAmount = calculateOrderTotal();
            placementDate = LocalDateTime.now();
        }
        return this;
    }
}
