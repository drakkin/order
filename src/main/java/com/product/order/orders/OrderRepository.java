package com.product.order.orders;

import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

public interface OrderRepository extends CrudRepository<Order,Long> {

    Iterable<Order> findAllByPlacementDateBetween(LocalDateTime start, LocalDateTime end);
    Optional<Order> findByIdAndPlacementDateIsNull(Long orderId);
}
