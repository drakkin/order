package com.product.order.orders;

import com.product.order.product.Product;
import com.product.order.product.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

@Transactional
@Service
@Slf4j
public class OrderService {

    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;

    public OrderService(OrderRepository orderRepository, ProductRepository productRepository) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
    }

    public Iterable<Order> findOrdersInPeriod(LocalDate start, LocalDate end) {
        return orderRepository.findAllByPlacementDateBetween(start.atStartOfDay(), end.atStartOfDay());
    }

    public Order createOrder(Order order) {
        order.setPlacementDate(null);
        return orderRepository.save(order);
    }

    public BigDecimal calculateTotal(Long orderId) {
        Optional<Order> byId = orderRepository.findById(orderId);
        if (byId.isPresent()) {
            Order order = byId.get();

            return order.calculateOrderTotal();
        }
        return null;
    }

    public Order placeOrder(Long orderId) {
        Optional<Order> byId = orderRepository.findById(orderId);
        if (byId.isPresent()) {
            Order order = byId.get();

            return orderRepository.save(order.place());
        }
        return null;
    }

    public Order addProduct(Long orderId, Long productId) {
        Optional<Order> byId = orderRepository.findByIdAndPlacementDateIsNull(orderId);
        if (byId.isPresent()) {

            Order order = byId.get();
            Optional<Product> product = productRepository.findById(productId);
            if (product.isPresent()) {
                order.getProductsInOrder().add(product.get());
                return orderRepository.save(order);
            }
            return order;
        }
        return null;
    }
}
