package com.product.order.orders;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/orders")
public class OrderController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping
    Iterable<Order> getOrdersInPeriod(@RequestParam("start") String start, @RequestParam("end") String end){
        return orderService.findOrdersInPeriod(LocalDate.parse(start),LocalDate.parse(end));
    }
    @PostMapping
    Order createOrder(@Valid @RequestBody Order order){
        return orderService.createOrder(order);
    }
    @GetMapping("{orderId}/total")
    BigDecimal total(Long orderId){
        return orderService.calculateTotal(orderId);
    }

    @PutMapping("{orderId}/place")
    Order placeOrder(@PathVariable Long orderId){
        return orderService.placeOrder(orderId);
    }
    @PostMapping("{orderId}/{productId}")
    Order addProduct(@PathVariable Long orderId,@PathVariable Long prodictId){
        return orderService.addProduct(orderId,prodictId);
    }


}
