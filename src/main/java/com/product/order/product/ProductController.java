package com.product.order.product;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductRepository productRepository;

    @GetMapping
    Iterable<Product> getProducts(){
       return productRepository.findAll();
    }
    @PostMapping
    Product updateProduct(@RequestBody Product body){
        body.setProductId(null);
        return productRepository.save(body);
    }
    @PutMapping("/{id}")
    Product updateProduct(@PathVariable Long id, @RequestBody Product body){
        body.setProductId(id);
        return productRepository.save(body);
    }
}
