package com.product.order.orders;

import com.product.order.product.Product;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;


public class OrderTest {

    @Test
    public void shouldCalculatoTotalAs0WhenNoProducts() {
        //Given
        Order order = new Order();
        //WHEN
        BigDecimal bigDecimal = order.calculateOrderTotal();
        //THEN
        assertThat(bigDecimal).isZero();

    }
    @Test
    public void shouldCalculatoTotalCorrectly() {
        //Given
        Order order = new Order();
        order.getProductsInOrder().add(Product.builder().price(BigDecimal.ONE).build());
        order.getProductsInOrder().add(Product.builder().price(BigDecimal.TEN).build());
        order.getProductsInOrder().add(Product.builder().price(BigDecimal.valueOf(5)).build());
        //WHEN
        BigDecimal bigDecimal = order.calculateOrderTotal();
        //THEN
        assertThat(bigDecimal).isEqualTo("16");

    }

    @Test
    public void shouldSetOrdeAmountWhenPlaced() {
        //Given
        Order order = new Order();
        order.getProductsInOrder().add(Product.builder().price(BigDecimal.ONE).build());
        order.getProductsInOrder().add(Product.builder().price(BigDecimal.TEN).build());
        order.getProductsInOrder().add(Product.builder().price(BigDecimal.valueOf(5)).build());
        //WHEN
        order.place();
        //THEN
        assertThat(order.getOrderAmount()).isEqualTo("16");

    }

    @Test
    public void shouldNotChangeValueEvenAfterProductPriceChanged() {
        //Given
        Order order = new Order();
        Product first = Product.builder().price(BigDecimal.ONE).build();
        order.getProductsInOrder().add(first);
        order.getProductsInOrder().add(Product.builder().price(BigDecimal.TEN).build());
        order.getProductsInOrder().add(Product.builder().price(BigDecimal.valueOf(5)).build());

        //WHEN
        order.place();
        first.setPrice(BigDecimal.valueOf(100));
        //THEN
        assertThat(order.calculateOrderTotal()).isEqualTo("16");

    }

}