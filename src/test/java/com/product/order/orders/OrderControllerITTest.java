package com.product.order.orders;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class OrderControllerITTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getOrdersInPeriod() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/orders").param("start",LocalDate.now().minus(1, ChronoUnit.DAYS).toString())
                .param("end",LocalDate.now().toString())).andExpect(
                MockMvcResultMatchers.status().is(200)
        );

    }
    @Test
    public void createOrder() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.post("/orders").content("{\"email\":\"jon.doe@gmail.com\"}").contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(
                MockMvcResultMatchers.status().is(200)
        );

    }

}