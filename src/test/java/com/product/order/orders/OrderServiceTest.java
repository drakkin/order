package com.product.order.orders;

import com.product.order.product.Product;
import com.product.order.product.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class OrderServiceTest {

    OrderService orderService;
    OrderRepository orderRepository;
    ProductRepository productRepository;


    @Before
    public void setUp(){
        orderRepository = mock(OrderRepository.class);
        productRepository = mock(ProductRepository.class);
        orderService = new OrderService(orderRepository,productRepository);
    }

    @Test
    public void shouldCreateOrderAndIgnorePlacementDate(){
        //Given
        Order order= new Order();
        order.setPlacementDate(LocalDateTime.now());
        //When
        orderService.createOrder(order);
        //Then
        ArgumentCaptor<Order> orderArgumentCaptor = ArgumentCaptor.forClass(Order.class);
        verify(orderRepository).save(orderArgumentCaptor.capture());
        assertThat(orderArgumentCaptor.getValue()).hasFieldOrPropertyWithValue("placementDate",null);
    }

    @Test
    public void shouldCallMethodForFinding(){
        //Given
        LocalDate start= LocalDate.now().minus(10, ChronoUnit.DAYS);
        LocalDate end= LocalDate.now().minus(2, ChronoUnit.DAYS);
        //When
        orderService.findOrdersInPeriod(start,end);
        //Then
        verify(orderRepository).findAllByPlacementDateBetween(start.atStartOfDay(),end.atStartOfDay());
    }

    @Test
    public void shouldReturnNullWhenOrderCalculatedDoesntExist(){
        //Given

        //When
        BigDecimal bigDecimal = orderService.calculateTotal(1L);
        //Then
       assertThat(bigDecimal).isNull();
    }

    @Test
    public void shouldReturnZeroWhenOrderCalculatedExistAndIsEmpty(){
        //Given
        Order order= new Order();
        when(orderRepository.findById(anyLong())).thenReturn(Optional.of(order));
        //When
        BigDecimal bigDecimal = orderService.calculateTotal(1L);
        //Then
        assertThat(bigDecimal).isZero();
    }
    @Test
    public void shouldReturnOrderAmountWhenOrderCalculatedExistAndIsPlaced(){
        //Given
        Order order= new Order();
        order.setPlacementDate(LocalDateTime.now());
        order.setOrderAmount(BigDecimal.valueOf(12));
        when(orderRepository.findById(anyLong())).thenReturn(Optional.of(order));
        //When
        BigDecimal bigDecimal = orderService.calculateTotal(1L);
        //Then
        assertThat(bigDecimal).isEqualTo("12");
    }

    @Test
    public void shouldReturnCalculatedAmountWhenOrderExistAndIsNotPlaced(){
        //Given
        Order order= new Order();
        order.setOrderAmount(BigDecimal.valueOf(12));
        order.getProductsInOrder().add(Product.builder().price(BigDecimal.valueOf(16)).build());
        when(orderRepository.findById(anyLong())).thenReturn(Optional.of(order));
        //When
        BigDecimal bigDecimal = orderService.calculateTotal(1L);
        //Then
        assertThat(bigDecimal).isEqualTo("16");
    }
    @Test
    public void shouldReturnNullWhenOrderPlacedDoesntExist(){
        //Given

        //When
        Order order = orderService.placeOrder(1L);
        //Then
        assertThat(order).isNull();
    }


    @Test
    public void shouldReturnOrderWhenOrderExists(){
        //Given
        Order order = new Order();
        when(orderRepository.findById(anyLong())).thenReturn(Optional.of(order));

        when(orderRepository.save(any())).thenAnswer(i -> i.getArgument(0));

        //When
        Order order2 = orderService.placeOrder(1L);
        //Then
        assertThat(order2).isNotNull().extracting(Order::getPlacementDate).isNotNull();
    }

    @Test
    public void shouldReturnOrderWhenOrderAlreadyPlacedExists(){
        //Given
        Order order = new Order();
        order.setOrderAmount(BigDecimal.TEN);
        order.setPlacementDate(LocalDateTime.now().minus(10,ChronoUnit.DAYS));
        when(orderRepository.findById(anyLong())).thenReturn(Optional.of(order));

        when(orderRepository.save(any())).thenAnswer(i -> i.getArgument(0));

        //When
        Order order2 = orderService.placeOrder(1L);
        //Then
        assertThat(order2).isNotNull().extracting(Order::getPlacementDate).matches((it -> ((LocalDateTime)it).isBefore(LocalDateTime.now().minus(9,ChronoUnit.DAYS))));
    }


    @Test
    public void shouldReturnNullWhenOrderDoesntExistForAddingProduct(){
        //Given

        //When
        Order order = orderService.addProduct(1L,2L);
        //Then
        assertThat(order).isNull();
    }

    @Test
    public void shouldReturnOrderWhenOrderExistsButProductDoesnt(){
        //Given
        Order order = new Order();
        when(orderRepository.findByIdAndPlacementDateIsNull(anyLong())).thenReturn(Optional.of(order));
        when(orderRepository.save(any())).thenAnswer(i -> i.getArgument(0));

        //When
        Order order2 = orderService.addProduct(1L,2L);
        //Then
        assertThat(order2).isEqualTo(order);
    }

    @Test
    public void shouldAddProductOrderWhenOrderAndProductExist(){
        //Given
        Order order = new Order();
        Product product= Product.builder().price(BigDecimal.ONE).build();
        when(orderRepository.findByIdAndPlacementDateIsNull(anyLong())).thenReturn(Optional.of(order));
        when(orderRepository.save(any())).thenAnswer(i -> i.getArgument(0));
        when(productRepository.findById(any())).thenReturn(Optional.of(product));

        //When
        Order order2 = orderService.addProduct(1L,2L);
        //Then
        assertThat(order2).extracting(Order::getProductsInOrder).matches(it -> !((List)it).isEmpty());
    }
}