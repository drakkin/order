package com.product.order.orders;

import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class OrderControllerTest {
    OrderService orderService=mock(OrderService.class);
    OrderController orderController = new OrderController(orderService);

    @Test
    public void getOrdersInPeriod() {
        //WHEN
        orderController.getOrdersInPeriod(LocalDate.now().toString(), LocalDate.now().toString());
        //EXPECT
        verify(orderService).findOrdersInPeriod(any(),any());
    }

    @Test
    public void createOrder() {
        //WHEN
        orderController.createOrder(new Order());
        //EXPECT
        verify(orderService).createOrder(any());
    }

    @Test
    public void total() {
        //WHEN
        orderController.total(1L);
        //EXPECT
        verify(orderService).calculateTotal(1L);
    }

    @Test
    public void placeOrder() {
        //WHEN
        orderController.placeOrder(1L);
        //EXPECT
        verify(orderService).placeOrder(1L);
    }

    @Test
    public void addProduct() {
        //WHEN
        orderController.addProduct(1L,2L);
        //EXPECT
        verify(orderService).addProduct(1L,2L);
    }
}