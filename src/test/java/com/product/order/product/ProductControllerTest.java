package com.product.order.product;

import org.junit.Test;

import java.math.BigDecimal;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ProductControllerTest {

    @Test
    public void shouldCreateNewProduct(){
        //GIVEN
        ProductRepository productRepository= mock(ProductRepository.class);
        ProductController productController= new ProductController(productRepository);
        Product product = new Product();
        product.setPrice(BigDecimal.ONE);
        product.setName("name");
        //WHEN
        Product product1 = productController.updateProduct(product);
        //THEN
        verify(productRepository).save(product);


    }

    @Test
    public void shouldUpdateProduct(){
        //GIVEN
        ProductRepository productRepository= mock(ProductRepository.class);
        ProductController productController= new ProductController(productRepository);
        Product product = new Product();
        product.setPrice(BigDecimal.ONE);
        product.setName("name");
        //WHEN
        Product product1 = productController.updateProduct(1L,product);
        //THEN
        verify(productRepository).save(product);


    }

    @Test
    public void shouldReturnAllProducts(){
        //GIVEN
        ProductRepository productRepository= mock(ProductRepository.class);
        ProductController productController= new ProductController(productRepository);
        //WHEN
        productController.getProducts();
        //THEN
        verify(productRepository).findAll();


    }


}