package com.product.order.repository;

import com.product.order.product.Product;
import com.product.order.product.ProductRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class ProductRepositoryTest {


    @Autowired
    ProductRepository repository;

    @Test
    public void shouldReturnASingleProduct(){
        //GIVEN
        Product product= new Product();
        product.setName("Product1");
        product.setPrice(BigDecimal.ONE);
        //WHEN
        Product save = repository.save(product);
        //THEN
        assertThat(save)
                .isEqualToIgnoringGivenFields(product,"id")
                .hasNoNullFieldsOrProperties();
    }
}